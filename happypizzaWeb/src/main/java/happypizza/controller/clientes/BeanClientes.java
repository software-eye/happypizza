package happypizza.controller.clientes;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import happypizza.controller.JSFUtil;
import happypizza.controller.seguridades.BeanSegLogin;
import happypizza.model.clientes.managers.ManagerClientes;
import happypizza.model.core.entities.SegModulo;
import happypizza.model.core.entities.SerCliente;

@Named
@SessionScoped
public class BeanClientes implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerClientes managerClientes;
	
	private List<SerCliente> listaClientes;
	private SerCliente nuevoCliente;
	private SerCliente edicionCliente;
	
	@Inject
	private BeanSegLogin beanSegLogin;
	
	public BeanClientes() {
		//TODO Auto-generated constructor stub
	}
	
	public String actionMenuClientes() {
		
		listaClientes = managerClientes.findAllClientes();
		return "clientes";
	}
	
	public void actionListenerInsertarNuevoCliente() {
		try {
			managerClientes.insertarCliente(nuevoCliente);
			listaClientes=managerClientes.findAllClientes();
			nuevoCliente=new SerCliente();
			JSFUtil.crearMensajeINFO("Cliente insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerCargarEdicion(SerCliente cliente) {
		edicionCliente=cliente;
	}
	
	
	public void actionListenerGuardarEdicionCliente() {
		try {
			managerClientes.actualizarCliente(edicionCliente);
			JSFUtil.crearMensajeINFO("Cliente editado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	
	public void actionListenerEliminarUsuario(int idSerCliente) {
		try {
			managerClientes.eliminarCliente(idSerCliente);
			listaClientes = managerClientes.findAllClientes();
			JSFUtil.crearMensajeINFO("Cliente eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<SerCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<SerCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public SerCliente getNuevoCliente() {
		return nuevoCliente;
	}

	public void setNuevoCliente(SerCliente nuevoCliente) {
		this.nuevoCliente = nuevoCliente;
	}

	public SerCliente getEdicionCliente() {
		return edicionCliente;
	}

	public void setEdicionCliente(SerCliente edicionCliente) {
		this.edicionCliente = edicionCliente;
	}

	public BeanSegLogin getBeanSegLogin() {
		return beanSegLogin;
	}

	public void setBeanSegLogin(BeanSegLogin beanSegLogin) {
		this.beanSegLogin = beanSegLogin;
	}
	
}
