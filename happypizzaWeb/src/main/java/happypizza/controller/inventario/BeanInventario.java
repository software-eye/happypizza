package happypizza.controller.inventario;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import happypizza.controller.JSFUtil;
import happypizza.controller.seguridades.BeanSegLogin;
import happypizza.model.clientes.managers.ManagerClientes;
import happypizza.model.core.entities.InvArticulo;
import happypizza.model.core.entities.SegModulo;
import happypizza.model.core.entities.SerCliente;
import happypizza.model.inventario.managers.ManagerInventario;

@Named
@SessionScoped
public class BeanInventario implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerInventario managerInventario;

	private List<InvArticulo> listaArticulos;
	private List listStock;
	private InvArticulo nuevoArticulo;
	private InvArticulo edicionArticulo;

	@Inject
	private BeanSegLogin beanSegLogin;

	@PersistenceContext
	private EntityManager em;

	public BeanInventario() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		nuevoArticulo = new InvArticulo();
		edicionArticulo = new InvArticulo();
		listaArticulos = managerInventario.findAllArticulos();
		
	}

	public List stockArticulos() {
		Query q;
		List listado;
		String sentenciaJPQL;
		sentenciaJPQL = "SELECT o FROM InvArticulo a INNER JOIN a.InvArticuloDetalle d";

		q = em.createQuery(sentenciaJPQL);
		listado = q.getResultList();
		return listado;
	}

	public void actionListenerInsertarNuevoArticulo() {
		try {
			managerInventario.insertarArticulo(nuevoArticulo);
			listaArticulos = managerInventario.findAllArticulos();
			nuevoArticulo = new InvArticulo();
			JSFUtil.crearMensajeINFO("Articulo insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
//
//	public void actionListenerCargarEdicion(SerCliente cliente) {
//		edicionCliente = cliente;
//	}
//
//	public void actionListenerGuardarEdicionCliente() {
//		try {
//			managerClientes.actualizarCliente(edicionCliente);
//			JSFUtil.crearMensajeINFO("Cliente editado.");
//		} catch (Exception e) {
//			JSFUtil.crearMensajeERROR(e.getMessage());
//			e.printStackTrace();
//		}
//
//	}
//
//	public void actionListenerEliminarUsuario(int idSerCliente) {
//		try {
//			managerClientes.eliminarCliente(idSerCliente);
//			listaClientes = managerClientes.findAllClientes();
//			JSFUtil.crearMensajeINFO("Cliente eliminado");
//		} catch (Exception e) {
//			JSFUtil.crearMensajeERROR(e.getMessage());
//			e.printStackTrace();
//		}
//	}

	public BeanSegLogin getBeanSegLogin() {
		return beanSegLogin;
	}

	public List<InvArticulo> getListaArticulos() {
		return listaArticulos;
	}

	public void setListaArticulos(List<InvArticulo> listaArticulos) {
		this.listaArticulos = listaArticulos;
	}

	public InvArticulo getNuevoArticulo() {
		return nuevoArticulo;
	}

	public void setNuevoArticulo(InvArticulo nuevoArticulo) {
		this.nuevoArticulo = nuevoArticulo;
	}

	public InvArticulo getEdicionArticulo() {
		return edicionArticulo;
	}

	public void setEdicionArticulo(InvArticulo edicionArticulo) {
		this.edicionArticulo = edicionArticulo;
	}

	public void setBeanSegLogin(BeanSegLogin beanSegLogin) {
		this.beanSegLogin = beanSegLogin;
	}

}
