package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the fin_producto database table.
 * 
 */
@Entity
@Table(name="fin_producto")
@NamedQuery(name="FinProducto.findAll", query="SELECT f FROM FinProducto f")
public class FinProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fin_producto")
	private Integer idFinProducto;

	private String categoria;

	private String descripcion;

	private String nombre;

	private BigDecimal precio;

	//bi-directional many-to-one association to FinFacturacionDetalle
	@OneToMany(mappedBy="finProducto")
	private List<FinFacturacionDetalle> finFacturacionDetalles;

	public FinProducto() {
	}

	public Integer getIdFinProducto() {
		return this.idFinProducto;
	}

	public void setIdFinProducto(Integer idFinProducto) {
		this.idFinProducto = idFinProducto;
	}

	public String getCategoria() {
		return this.categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public List<FinFacturacionDetalle> getFinFacturacionDetalles() {
		return this.finFacturacionDetalles;
	}

	public void setFinFacturacionDetalles(List<FinFacturacionDetalle> finFacturacionDetalles) {
		this.finFacturacionDetalles = finFacturacionDetalles;
	}

	public FinFacturacionDetalle addFinFacturacionDetalle(FinFacturacionDetalle finFacturacionDetalle) {
		getFinFacturacionDetalles().add(finFacturacionDetalle);
		finFacturacionDetalle.setFinProducto(this);

		return finFacturacionDetalle;
	}

	public FinFacturacionDetalle removeFinFacturacionDetalle(FinFacturacionDetalle finFacturacionDetalle) {
		getFinFacturacionDetalles().remove(finFacturacionDetalle);
		finFacturacionDetalle.setFinProducto(null);

		return finFacturacionDetalle;
	}

}