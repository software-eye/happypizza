package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the egr_compra_detalle database table.
 * 
 */
@Entity
@Table(name="egr_compra_detalle")
@NamedQuery(name="EgrCompraDetalle.findAll", query="SELECT e FROM EgrCompraDetalle e")
public class EgrCompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_egr_destalle")
	private Integer idInvEgrDestalle;

	private BigDecimal cantidad;

	private BigDecimal total;

	//bi-directional many-to-one association to EgrCompra
	@ManyToOne
	@JoinColumn(name="id_egr_compra")
	private EgrCompra egrCompra;

	//bi-directional many-to-one association to InvArticulo
	@ManyToOne
	@JoinColumn(name="id_inv_articulo")
	private InvArticulo invArticulo;

	public EgrCompraDetalle() {
	}

	public Integer getIdInvEgrDestalle() {
		return this.idInvEgrDestalle;
	}

	public void setIdInvEgrDestalle(Integer idInvEgrDestalle) {
		this.idInvEgrDestalle = idInvEgrDestalle;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public EgrCompra getEgrCompra() {
		return this.egrCompra;
	}

	public void setEgrCompra(EgrCompra egrCompra) {
		this.egrCompra = egrCompra;
	}

	public InvArticulo getInvArticulo() {
		return this.invArticulo;
	}

	public void setInvArticulo(InvArticulo invArticulo) {
		this.invArticulo = invArticulo;
	}

}