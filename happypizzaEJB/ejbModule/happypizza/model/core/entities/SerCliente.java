package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ser_cliente database table.
 * 
 */
@Entity
@Table(name="ser_cliente")
@NamedQuery(name="SerCliente.findAll", query="SELECT s FROM SerCliente s")
public class SerCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ser_cliente", unique=true, nullable=false)
	private Integer idSerCliente;

	@Column(nullable=false, length=50)
	private String apellidos;

	private String cedula;

	@Column(nullable=false, length=70)
	private String direccion;

	@Column(nullable=false, length=50)
	private String nombres;

	@Column(nullable=false, length=10)
	private String telefono;

	//bi-directional many-to-one association to FinFacturacionCabecera
	@OneToMany(mappedBy="serCliente")
	private List<FinFacturacionCabecera> finFacturacionCabeceras;

	//bi-directional many-to-one association to SerEvento
	@OneToMany(mappedBy="serCliente")
	private List<SerEvento> serEventos;

	public SerCliente() {
	}

	public Integer getIdSerCliente() {
		return this.idSerCliente;
	}

	public void setIdSerCliente(Integer idSerCliente) {
		this.idSerCliente = idSerCliente;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<FinFacturacionCabecera> getFinFacturacionCabeceras() {
		return this.finFacturacionCabeceras;
	}

	public void setFinFacturacionCabeceras(List<FinFacturacionCabecera> finFacturacionCabeceras) {
		this.finFacturacionCabeceras = finFacturacionCabeceras;
	}

	public FinFacturacionCabecera addFinFacturacionCabecera(FinFacturacionCabecera finFacturacionCabecera) {
		getFinFacturacionCabeceras().add(finFacturacionCabecera);
		finFacturacionCabecera.setSerCliente(this);

		return finFacturacionCabecera;
	}

	public FinFacturacionCabecera removeFinFacturacionCabecera(FinFacturacionCabecera finFacturacionCabecera) {
		getFinFacturacionCabeceras().remove(finFacturacionCabecera);
		finFacturacionCabecera.setSerCliente(null);

		return finFacturacionCabecera;
	}

	public List<SerEvento> getSerEventos() {
		return this.serEventos;
	}

	public void setSerEventos(List<SerEvento> serEventos) {
		this.serEventos = serEventos;
	}

	public SerEvento addSerEvento(SerEvento serEvento) {
		getSerEventos().add(serEvento);
		serEvento.setSerCliente(this);

		return serEvento;
	}

	public SerEvento removeSerEvento(SerEvento serEvento) {
		getSerEventos().remove(serEvento);
		serEvento.setSerCliente(null);

		return serEvento;
	}

}