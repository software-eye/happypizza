package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the inv_articulo_detalle database table.
 * 
 */
@Entity
@Table(name="inv_articulo_detalle")
@NamedQuery(name="InvArticuloDetalle.findAll", query="SELECT i FROM InvArticuloDetalle i")
public class InvArticuloDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_articulo_detalle")
	private Integer idInvArticuloDetalle;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_expiracion")
	private Date fechaExpiracion;

	private BigDecimal stock;

	//bi-directional many-to-one association to InvArticulo
	@ManyToOne
	@JoinColumn(name="id_inv_articulo")
	private InvArticulo invArticulo;

	public InvArticuloDetalle() {
	}

	public Integer getIdInvArticuloDetalle() {
		return this.idInvArticuloDetalle;
	}

	public void setIdInvArticuloDetalle(Integer idInvArticuloDetalle) {
		this.idInvArticuloDetalle = idInvArticuloDetalle;
	}

	public Date getFechaExpiracion() {
		return this.fechaExpiracion;
	}

	public void setFechaExpiracion(Date fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

	public BigDecimal getStock() {
		return this.stock;
	}

	public void setStock(BigDecimal stock) {
		this.stock = stock;
	}

	public InvArticulo getInvArticulo() {
		return this.invArticulo;
	}

	public void setInvArticulo(InvArticulo invArticulo) {
		this.invArticulo = invArticulo;
	}

}