package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the egr_compra database table.
 * 
 */
@Entity
@Table(name="egr_compra")
@NamedQuery(name="EgrCompra.findAll", query="SELECT e FROM EgrCompra e")
public class EgrCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_egr_compra")
	private Integer idEgrCompra;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_venta")
	private Date fechaVenta;

	private BigDecimal total;

	//bi-directional many-to-one association to EgrCompraDetalle
	@OneToMany(mappedBy="egrCompra")
	private List<EgrCompraDetalle> egrCompraDetalles;

	public EgrCompra() {
	}

	public Integer getIdEgrCompra() {
		return this.idEgrCompra;
	}

	public void setIdEgrCompra(Integer idEgrCompra) {
		this.idEgrCompra = idEgrCompra;
	}

	public Date getFechaVenta() {
		return this.fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<EgrCompraDetalle> getEgrCompraDetalles() {
		return this.egrCompraDetalles;
	}

	public void setEgrCompraDetalles(List<EgrCompraDetalle> egrCompraDetalles) {
		this.egrCompraDetalles = egrCompraDetalles;
	}

	public EgrCompraDetalle addEgrCompraDetalle(EgrCompraDetalle egrCompraDetalle) {
		getEgrCompraDetalles().add(egrCompraDetalle);
		egrCompraDetalle.setEgrCompra(this);

		return egrCompraDetalle;
	}

	public EgrCompraDetalle removeEgrCompraDetalle(EgrCompraDetalle egrCompraDetalle) {
		getEgrCompraDetalles().remove(egrCompraDetalle);
		egrCompraDetalle.setEgrCompra(null);

		return egrCompraDetalle;
	}

}