package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the inv_articulo database table.
 * 
 */
@Entity
@Table(name="inv_articulo")
@NamedQuery(name="InvArticulo.findAll", query="SELECT i FROM InvArticulo i")
public class InvArticulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_articulo")
	private Integer idInvArticulo;

	private String categoria;

	private String marca;

	private String nombre;

	private BigDecimal precio;

	//bi-directional many-to-one association to EgrCompraDetalle
	@OneToMany(mappedBy="invArticulo")
	private List<EgrCompraDetalle> egrCompraDetalles;

	//bi-directional many-to-one association to InvArticuloDetalle
	@OneToMany(mappedBy="invArticulo")
	private List<InvArticuloDetalle> invArticuloDetalles;

	public InvArticulo() {
	}

	public Integer getIdInvArticulo() {
		return this.idInvArticulo;
	}

	public void setIdInvArticulo(Integer idInvArticulo) {
		this.idInvArticulo = idInvArticulo;
	}

	public String getCategoria() {
		return this.categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public List<EgrCompraDetalle> getEgrCompraDetalles() {
		return this.egrCompraDetalles;
	}

	public void setEgrCompraDetalles(List<EgrCompraDetalle> egrCompraDetalles) {
		this.egrCompraDetalles = egrCompraDetalles;
	}

	public EgrCompraDetalle addEgrCompraDetalle(EgrCompraDetalle egrCompraDetalle) {
		getEgrCompraDetalles().add(egrCompraDetalle);
		egrCompraDetalle.setInvArticulo(this);

		return egrCompraDetalle;
	}

	public EgrCompraDetalle removeEgrCompraDetalle(EgrCompraDetalle egrCompraDetalle) {
		getEgrCompraDetalles().remove(egrCompraDetalle);
		egrCompraDetalle.setInvArticulo(null);

		return egrCompraDetalle;
	}

	public List<InvArticuloDetalle> getInvArticuloDetalles() {
		return this.invArticuloDetalles;
	}

	public void setInvArticuloDetalles(List<InvArticuloDetalle> invArticuloDetalles) {
		this.invArticuloDetalles = invArticuloDetalles;
	}

	public InvArticuloDetalle addInvArticuloDetalle(InvArticuloDetalle invArticuloDetalle) {
		getInvArticuloDetalles().add(invArticuloDetalle);
		invArticuloDetalle.setInvArticulo(this);

		return invArticuloDetalle;
	}

	public InvArticuloDetalle removeInvArticuloDetalle(InvArticuloDetalle invArticuloDetalle) {
		getInvArticuloDetalles().remove(invArticuloDetalle);
		invArticuloDetalle.setInvArticulo(null);

		return invArticuloDetalle;
	}

}