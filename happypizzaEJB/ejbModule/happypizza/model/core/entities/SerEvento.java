package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the ser_evento database table.
 * 
 */
@Entity
@Table(name="ser_evento")
@NamedQuery(name="SerEvento.findAll", query="SELECT s FROM SerEvento s")
public class SerEvento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ser_evento")
	private Integer idSerEvento;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private Time hora;

	private String nombre;

	//bi-directional many-to-one association to SerCliente
	@ManyToOne
	@JoinColumn(name="id_ser_cliente")
	private SerCliente serCliente;

	public SerEvento() {
	}

	public Integer getIdSerEvento() {
		return this.idSerEvento;
	}

	public void setIdSerEvento(Integer idSerEvento) {
		this.idSerEvento = idSerEvento;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Time getHora() {
		return this.hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public SerCliente getSerCliente() {
		return this.serCliente;
	}

	public void setSerCliente(SerCliente serCliente) {
		this.serCliente = serCliente;
	}

}