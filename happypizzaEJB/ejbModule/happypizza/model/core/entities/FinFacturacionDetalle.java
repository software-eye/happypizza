package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the fin_facturacion_detalle database table.
 * 
 */
@Entity
@Table(name="fin_facturacion_detalle")
@NamedQuery(name="FinFacturacionDetalle.findAll", query="SELECT f FROM FinFacturacionDetalle f")
public class FinFacturacionDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fin_facturacion_detalle")
	private Integer idFinFacturacionDetalle;

	private BigDecimal cantidad;

	private BigDecimal subtotal;

	//bi-directional many-to-one association to FinFacturacionCabecera
	@ManyToOne
	@JoinColumn(name="id_fin_facturacion_cabecera")
	private FinFacturacionCabecera finFacturacionCabecera;

	//bi-directional many-to-one association to FinProducto
	@ManyToOne
	@JoinColumn(name="id_fin_producto")
	private FinProducto finProducto;

	public FinFacturacionDetalle() {
	}

	public Integer getIdFinFacturacionDetalle() {
		return this.idFinFacturacionDetalle;
	}

	public void setIdFinFacturacionDetalle(Integer idFinFacturacionDetalle) {
		this.idFinFacturacionDetalle = idFinFacturacionDetalle;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public FinFacturacionCabecera getFinFacturacionCabecera() {
		return this.finFacturacionCabecera;
	}

	public void setFinFacturacionCabecera(FinFacturacionCabecera finFacturacionCabecera) {
		this.finFacturacionCabecera = finFacturacionCabecera;
	}

	public FinProducto getFinProducto() {
		return this.finProducto;
	}

	public void setFinProducto(FinProducto finProducto) {
		this.finProducto = finProducto;
	}

}