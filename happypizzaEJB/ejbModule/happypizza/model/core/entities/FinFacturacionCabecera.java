package happypizza.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the fin_facturacion_cabecera database table.
 * 
 */
@Entity
@Table(name="fin_facturacion_cabecera")
@NamedQuery(name="FinFacturacionCabecera.findAll", query="SELECT f FROM FinFacturacionCabecera f")
public class FinFacturacionCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fin_facturacion_cabecera")
	private Integer idFinFacturacionCabecera;

	private BigDecimal descuento;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_venta")
	private Date fechaVenta;

	private BigDecimal iva;

	private BigDecimal subtotal;

	private BigDecimal total;

	//bi-directional many-to-one association to SerCliente
	@ManyToOne
	@JoinColumn(name="id_ser_cliente")
	private SerCliente serCliente;

	//bi-directional many-to-one association to FinFacturacionDetalle
	@OneToMany(mappedBy="finFacturacionCabecera")
	private List<FinFacturacionDetalle> finFacturacionDetalles;

	public FinFacturacionCabecera() {
	}

	public Integer getIdFinFacturacionCabecera() {
		return this.idFinFacturacionCabecera;
	}

	public void setIdFinFacturacionCabecera(Integer idFinFacturacionCabecera) {
		this.idFinFacturacionCabecera = idFinFacturacionCabecera;
	}

	public BigDecimal getDescuento() {
		return this.descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public Date getFechaVenta() {
		return this.fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public SerCliente getSerCliente() {
		return this.serCliente;
	}

	public void setSerCliente(SerCliente serCliente) {
		this.serCliente = serCliente;
	}

	public List<FinFacturacionDetalle> getFinFacturacionDetalles() {
		return this.finFacturacionDetalles;
	}

	public void setFinFacturacionDetalles(List<FinFacturacionDetalle> finFacturacionDetalles) {
		this.finFacturacionDetalles = finFacturacionDetalles;
	}

	public FinFacturacionDetalle addFinFacturacionDetalle(FinFacturacionDetalle finFacturacionDetalle) {
		getFinFacturacionDetalles().add(finFacturacionDetalle);
		finFacturacionDetalle.setFinFacturacionCabecera(this);

		return finFacturacionDetalle;
	}

	public FinFacturacionDetalle removeFinFacturacionDetalle(FinFacturacionDetalle finFacturacionDetalle) {
		getFinFacturacionDetalles().remove(finFacturacionDetalle);
		finFacturacionDetalle.setFinFacturacionCabecera(null);

		return finFacturacionDetalle;
	}

}