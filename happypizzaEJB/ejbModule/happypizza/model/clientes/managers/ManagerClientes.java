package happypizza.model.clientes.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import happypizza.model.auditoria.managers.ManagerAuditoria;
import happypizza.model.core.entities.InvArticulo;
import happypizza.model.core.entities.SerCliente;
import happypizza.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerClientes
 */
@Stateless
@LocalBean
public class ManagerClientes {

	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;

	/**
	 * Default constructor.
	 */
	public ManagerClientes() {
		// TODO Auto-generated constructor stub
	}

	
	public List<SerCliente> findAllClientes() {
		return mDAO.findAll(SerCliente.class, "apellidos");
	}


	public SerCliente findByIdSerCliente(int idSerCliente) throws Exception {
		return (SerCliente) mDAO.findById(SerCliente.class, idSerCliente);
	}

	public SerCliente insertarCliente(SerCliente nuevoCliente) throws Exception {
		SerCliente cliente = new SerCliente();
		cliente.setApellidos(nuevoCliente.getApellidos());
		cliente.setNombres(nuevoCliente.getNombres());
		cliente.setDireccion(nuevoCliente.getDireccion());
		cliente.setTelefono(nuevoCliente.getTelefono());
		mDAO.insertar(cliente);
		return cliente;

	}

	public void actualizarCliente(SerCliente edicionCliente) throws Exception {
		SerCliente cliente = (SerCliente) mDAO.findById(SerCliente.class, edicionCliente.getIdSerCliente());
		cliente.setApellidos(edicionCliente.getApellidos());
		cliente.setNombres(edicionCliente.getNombres());
		cliente.setDireccion(edicionCliente.getDireccion());
		cliente.setTelefono(edicionCliente.getTelefono());
		mDAO.actualizar(cliente);
	}

	public void eliminarCliente(int idSerCliente) throws Exception {

		SerCliente cliente = (SerCliente) mDAO.findById(SerCliente.class, idSerCliente);
		mDAO.eliminar(SerCliente.class, cliente.getIdSerCliente());
	}

}
